package logger;

import (
	"os"
	"fmt"
	"time"
	"runtime"
	"strconv"
)

var path = "log/application.log"

func CreateFile(){

	// Detect if file exists
	var _,err = os.Stat(path)
	if os.IsNotExist(err){
		var file,err = os.Create(path)
		if ( err != nil) {
			fmt.Println("Error - No se pudo crear el archivo de Log!.")
		}
		defer file.Close()
	}
}
func Write(message interface{}, caller int)  {
	// open file using READ & WRITE permission
	file, err := os.OpenFile(path, os.O_APPEND|os.O_RDWR, 0666)
	if (err != nil){
		fmt.Println("Error - no se pudo abrir el archivo de log")
		fmt.Println(err)
	}
	defer file.Close()
	time := time.Now()

	_,file_s,line,ok := runtime.Caller(caller)
	if ok {
		_, err = file.WriteString("message:" + message.(string) + "||file:" + file_s + "||line:" + strconv.Itoa(line) + "||time:"+ time.String() + "\n")
		if (err != nil){
			fmt.Println("Error - no se pudo escribir en el archivo de log")
			fmt.Println(err)
		}
		//fmt.Println("file: " + file_s + "-line:" + fmt.Sprintf("%v",line))
	}else{
		fmt.Println("No se pudo determinar de donde vino log - logger.go")
	}

	
}
func SaveLog(message interface{})  {
	Write(message,2)
}


func CheckError(err error) {
    if err != nil {
        Write("Error: " + err.Error(),2)
    }
}


